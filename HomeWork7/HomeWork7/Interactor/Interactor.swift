//
//  Interactor.swift
//  HomeWork7
//
//  Created by Nazar Starantsov on 02.11.2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//

import Foundation
import UIKit

class Interactor {
    var networkManager: NetworkServiceProtocol?
    var imageModel = ImageEntity()
    var output: PresenterOutputProtocol?
}

protocol InteractorInputProtocol {
    func getImage(_ state: ButtonType)
}

protocol InteractorOutputProtocol {
    func alertMessage(state: ButtonType, message: String)
    func sendMessage(image: UIImage)
}
