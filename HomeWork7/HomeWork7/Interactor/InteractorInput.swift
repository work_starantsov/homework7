//
//  InteractorInput.swift
//  HomeWork7
//
//  Created by Nazar Starantsov on 02.11.2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//

import Foundation
import UIKit

extension Interactor: InteractorInputProtocol {
    func getImage(_ state: ButtonType) {
        
        var alertMessage = ""
        
        switch state {
        case .save:
            
            networkManager?.downloadImage { image, error in
                DispatchQueue.main.async {
                    
                    guard let image = image else {
                        alertMessage = "\(String(describing: error!.localizedDescription))"
                        return
                    }
                
                    self.imageModel.image = image
                    alertMessage = "Загружено"
                    self.output?.prepareAlert(with: alertMessage, state: state)
                }
            }
        case .delete:
            self.imageModel.image = nil
            self.output?.prepareImage(image: UIImage(named: "blank")!)
            self.output?.prepareAlert(with: "Кэш очищен!", state: state)
        case .show:
            
            guard let image = imageModel.image else {
                self.output?.prepareAlert(with: "Картинка не загружена", state: state)
                return
            }
            self.output?.prepareImage(image: image)
        }
        
    }
    
    
}
