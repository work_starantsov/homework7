//
//  PresenterInput.swift
//  HomeWork7
//
//  Created by Nazar Starantsov on 02.11.2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//

import Foundation

extension Presenter: PresenterInputProtocol {
    func getImage(_ state: ButtonType) {
        interactorInput?.getImage(state)
    }
}
