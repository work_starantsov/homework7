//
//  PresenterOutput.swift
//  HomeWork7
//
//  Created by Nazar Starantsov on 02.11.2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//

import Foundation
import UIKit

extension Presenter: PresenterOutputProtocol {
    func prepareAlert(with text: String, state: ButtonType) {
        let alert = UIAlertController(title: nil, message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        
        self.viewOutput?.presentAlert(controller: alert)
        
    }
    
    func prepareImage(image: UIImage)
    {
        viewOutput?.showImage(image: image)
    }
}
