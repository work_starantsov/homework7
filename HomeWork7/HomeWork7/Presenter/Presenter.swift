//
//  Presenter.swift
//  HomeWork7
//
//  Created by Nazar Starantsov on 02.11.2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//

import Foundation
import UIKit

class Presenter {
    var interactorInput: InteractorInputProtocol? = nil
    weak var viewOutput: ImagePickControllerProtocol?
}

protocol PresenterInputProtocol {
    func getImage(_ state:ButtonType)
}


protocol PresenterOutputProtocol {
    func prepareAlert(with text: String, state: ButtonType)
    func prepareImage(image: UIImage)
}
