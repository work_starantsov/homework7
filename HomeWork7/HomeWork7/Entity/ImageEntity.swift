//
//  ImageEntity.swift
//  HomeWork7
//
//  Created by Nazar Starantsov on 02.11.2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//

import Foundation
import UIKit

struct ImageEntity {
    var image: UIImage?
    var accesabilityName: String?
}

