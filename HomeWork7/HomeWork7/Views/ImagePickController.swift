//
//  ViewController.swift
//  HomeWork7
//
//  Created by Nazar Starantsov on 02.11.2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//

import UIKit

class ImagePickController: UIViewController {
    
    var presenterInput: PresenterInputProtocol?
    
    lazy var imageView: UIImageView = {
        let image = UIImageView(frame: .zero)
        image.translatesAutoresizingMaskIntoConstraints = false
        image.clipsToBounds = true
        image.image = UIImage(named: "empty")
        image.layer.cornerRadius = 10
        return image
    }()
    
    lazy var saveButton: UIButton = {
        let button = UIButton(frame: .zero)
        button.translatesAutoresizingMaskIntoConstraints = false
         button.setTitle("Сохранить", for: .normal)
        button.backgroundColor = .lightGray
        button.layer.cornerRadius = 10
        return button
    }()
    
    lazy var deleteButton: UIButton = {
        let button = UIButton(frame: .zero)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Очистить кэш", for: .normal)
        button.backgroundColor = .lightGray
        button.layer.cornerRadius = 10
        return button
    }()
    
    lazy var showButton: UIButton = {
        let button = UIButton(frame: .zero)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Показать", for: .normal)
        button.backgroundColor = .lightGray
        button.layer.cornerRadius = 10
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.cyan.withAlphaComponent(0.5)
        addSubviews()
        setupConstraints()
        setupGestures()
    }
    
    func addSubviews() {
        view.addSubview(imageView)
        view.addSubview(saveButton)
        view.addSubview(deleteButton)
        view.addSubview(showButton)
    }
    
    func setupConstraints() {

        imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive =  true
        imageView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 30).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 150).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 150).isActive = true
        
        saveButton.leftAnchor.constraint(equalTo: imageView.leftAnchor).isActive = true
         saveButton.topAnchor.constraint(equalTo: imageView.bottomAnchor , constant: 20).isActive = true
        saveButton.widthAnchor.constraint(equalTo: imageView.widthAnchor).isActive = true
        
        showButton.leftAnchor.constraint(equalTo: saveButton.leftAnchor).isActive = true
        showButton.topAnchor.constraint(equalTo: saveButton.bottomAnchor , constant: 20).isActive = true
        showButton.widthAnchor.constraint(equalTo: imageView.widthAnchor).isActive = true
        
        deleteButton.leftAnchor.constraint(equalTo: showButton.leftAnchor).isActive = true
        deleteButton.topAnchor.constraint(equalTo: showButton.bottomAnchor , constant: 20).isActive = true
        deleteButton.widthAnchor.constraint(equalTo: imageView.widthAnchor).isActive = true
        
    }
    
    func setupGestures() {
        saveButton.addTarget(self, action: #selector(saveButtonClicked), for: .touchUpInside)
        showButton.addTarget(self, action: #selector(showButtonClicked), for: .touchUpInside)
        deleteButton.addTarget(self, action: #selector(deleteButtonClicked), for: .touchUpInside)
    }
    
    
    @objc func saveButtonClicked(_ sender: UIButton) {
        presenterInput?.getImage(.save)
    }
    
    @objc func showButtonClicked(_ sender: UIButton) {
         presenterInput?.getImage(.show)
    }
    
    @objc func deleteButtonClicked(_ sender: UIButton) {
         presenterInput?.getImage(.delete)
    }
}

extension ImagePickController: ImagePickControllerProtocol {
    func presentAlert(controller: UIAlertController) {
        self.present(controller, animated: true, completion: nil)
    }
    
    func showImage(image: UIImage){
        imageView.image = image
    }
}

protocol ImagePickControllerProtocol: class {
    func presentAlert(controller: UIAlertController)
    func showImage(image: UIImage)
}

enum ButtonType {
    case show
    case save
    case delete
}

