//
//  Assembly.swift
//  HW9
//
//  Created by Katherine on 01/11/2019.
//  Copyright © 2019 Katherine123. All rights reserved.
//

import Foundation

class Assembly {
    static func getImagePickController() -> ImagePickController {
        let vc = ImagePickController()
        let presenter = Presenter()
        let interactor = Interactor()
        
        interactor.networkManager = NetworkService()
        interactor.output = presenter
        presenter.interactorInput = interactor
        presenter.viewOutput = vc
        vc.presenterInput = presenter
        
        return vc
    }
}
