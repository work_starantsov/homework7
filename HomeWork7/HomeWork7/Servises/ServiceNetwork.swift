//
//  ImageLoaderControllerInteractorInput.swift
//  HW9
//
//  Created by Katherine on 01/11/2019.
//  Copyright © 2019 Katherine123. All rights reserved.
//

import UIKit

protocol NetworkServiceProtocol: class {
    
    func downloadImage(completion: @escaping (UIImage?, Error?) -> Void)
}

class NetworkService: NSObject, NetworkServiceProtocol {
    
    func downloadImage(completion: @escaping (UIImage?, Error?) -> Void) {
        //return completion(UIImage(named: "privacy")!, nil)
        guard let url = URL(string:"https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_92x30dp.png") else { return }

        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            if let currentError = error {
                completion(nil, currentError)
                return
            }

            guard let currentData = data else { return }
            let image = UIImage(data: currentData)
            completion(image, nil)
        }

        task.resume()
    }
}

